import { Quote } from './../data/quote.interface';
export class QuotesService {
    private favoriteQuotes: Quote[] = [];

    addQuoteToFavorites(quote: Quote) {
        this.favoriteQuotes.push(quote);
        console.log(quote);
    }

    removeQuoteFromFavorites(quote: Quote): Quote[] {
        const position = this.favoriteQuotes.findIndex(element => element.id == quote.id);
        this.favoriteQuotes.splice(position, 1);
        return this.getFavoritesQuotes();
    }

    getFavoritesQuotes(): Quote[] {
        return this.favoriteQuotes.slice(); // return just a copy, no the real array .
    }

    isQuoteFavorite(quote: Quote): Quote {
        return this.favoriteQuotes.find(element => quote.id == element.id);
    }

}