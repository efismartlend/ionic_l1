import { GridPage } from './../grid/grid';
import { LibraryPage } from './../library/library';
import { FavoritesPage } from './../favorites/favorites';
import { Component } from '@angular/core';

@Component({
    selector: 'page-tabs',
    template: `
    <ion-tabs>
        <ion-tab [root]="gridPage" tabTitle="Grid" tabIcon="logo-angular"></ion-tab>
        <ion-tab [root]="libraryPage" tabTitle="Library" tabIcon="book"></ion-tab>
        <ion-tab [root]="favoritesPage" tabTitle="Favorites" tabIcon="star"></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {

    gridPage: any = GridPage;
    favoritesPage: any = FavoritesPage;
    libraryPage: any = LibraryPage;

}
