import { SettingsService } from './../../services/settings.service';
import { QuotePage } from './../quote/quote';
import { QuotesService } from './../../services/quotes.service';
import { Quote } from './../../data/quote.interface';
import { Component } from '@angular/core';
import { ModalController, MenuController } from 'ionic-angular';

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {

  quotes: Quote[] = []

  constructor(
    private quotesService: QuotesService,
    private settingsService: SettingsService,
    private modalCtrl: ModalController,
    private menuCtrl: MenuController) { }

  onViewQuote(quote: Quote) {
    const modal = this.modalCtrl.create(QuotePage, quote);
    modal.present();


    modal.willEnter.subscribe(() => console.log("1. willEnter is called"));

    modal.didEnter.subscribe(() => console.log("2. didEnter is called"));

    modal.willLeave.subscribe(() => console.log("3. willLeave is called"));

    modal.onWillDismiss(remove => {
      console.log("4. onWillDismiss is called")
      if (remove) {
        this.onRemoveFromFavorites(quote);
      }
    });

    modal.didLeave.subscribe(() => console.log("5. didLeave is called"));

    modal.onDidDismiss(() => {
      console.log("6. onDidDismiss is called")
    });
  }

  getBackgroung() {
    return this.settingsService.isAltBackground() ? 'altQuoteBackground' : 'quoteBackground';
  }
  
  isAltBackground(): boolean{
    return this.settingsService.isAltBackground();
  }
  
  ionViewWillEnter() {
    console.log("Will Enter")
    this.quotes = this.quotesService.getFavoritesQuotes();
  }

  onOpenMenu() {
    this.menuCtrl.open();
  }

  onRemoveFromFavorites(quote: Quote) {
    this.quotes = this.quotesService.removeQuoteFromFavorites(quote);
  }

}
