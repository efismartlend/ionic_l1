import { QuotesService } from './../../services/quotes.service';
import { Quote } from './../../data/quote.interface';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html'
})
export class QuotesPage {

  quoteGroup: { category: string, quotes: Quote[], icon: string };

  constructor(private navCtrl: NavController,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private quotesService: QuotesService
  ) {
  }

  ionViewDidLoad() {
    this.quoteGroup = this.navParams.data;
  }

  onAddToFavorite(quote: Quote): void {
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      subTitle: 'Are you sure?',
      message: 'Are you sure you want to add the quote?',
      buttons: [{
        text: 'Yes',
        handler: () => this.quotesService.addQuoteToFavorites(quote)
      }, {
        text: 'No',
        role: 'cancel',
        handler: () => console.log('Canceled')
      }],
    })
    alert.present();
  }

  onRemoveFromFavorite(quote: Quote): void {
    this.quotesService.removeQuoteFromFavorites(quote)
  }

  isFavorite(quote: Quote): boolean {
    return this.quotesService.isQuoteFavorite(quote) != null;
  }

}
