import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html'
})
export class QuotePage {

  person: string;
  text: string;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) { }

  ionViewDidLoad() {
    this.person = this.navParams.get('person');
    this.text = this.navParams.get('text');
  }

  onClose(remove: boolean = false) {
    this.viewCtrl.dismiss(remove);
  }

}
