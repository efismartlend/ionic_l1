import { GridPage } from './../pages/grid/grid';
import { SettingsService } from './../services/settings.service';
import { QuotesService } from './../services/quotes.service';
import { TabsPage } from './../pages/tabs/tabs';
import { QuotesPage } from './../pages/quotes/quotes';
import { LibraryPage } from './../pages/library/library';
import { SettingsPage } from './../pages/settings/settings';
import { QuotePage } from './../pages/quote/quote';
import { FavoritesPage } from './../pages/favorites/favorites';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    FavoritesPage,
    QuotePage,
    QuotesPage,
    SettingsPage,
    LibraryPage,
    TabsPage,
    GridPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FavoritesPage,
    QuotePage,
    QuotesPage,
    SettingsPage,
    LibraryPage,
    TabsPage,
    GridPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    QuotesService,
    SettingsService
  ]
})
export class AppModule { }
