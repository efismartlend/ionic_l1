import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-buyout',
  templateUrl: 'buyout.html'
})
export class BuyoutPage implements OnInit {


  productData: { name: string, quantity: number };

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(): void {
    this.productData = this.navParams.data;
  }

  onConfirmPurchase() {
    this.navCtrl.popToRoot();
  }

}
