import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage implements OnInit {

  name: string;

  constructor(
    private navParams: NavParams,
    private navCtrl: NavController
  ) { }

  ngOnInit(): void {
    this.name = this.navParams.get('username');
  }

  onGoBack() {
    // this.navCtrl.pop(); ------------------ back one page
    this.navCtrl.popToRoot(); //  ------------------ back to the root page
  }
}
