import { ShopPage } from './../shop/shop';
import { UserPage } from './user/user';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-users',
  templateUrl: 'users.html'
})
export class UsersPage {

  constructor(private navCtrl: NavController) {

  }

  onLoadUser(name: string) {
    this.navCtrl.push(UserPage, { username: name });
  }


  onGoToShop() {
    this.navCtrl.push(ShopPage);
  }


  ionViewCanEnter(): boolean | Promise<void> {
    console.log('ionViewCanEnter');
    const rnd = Math.random();
    // Before leave the page before we checking here if can we enter to this page 
    // LIKE GUARD IN ANGULAR 2 
    return rnd > 0.1;
  }


  ionViewDidLoad() {
    /// inialisiasion
    /// Page has loaded => Not fired when cached (if we go and then come back.) 
    console.log('ionViewDidLoad');
  }

  ionViewWillEnter() {
    // Page is about to enter and become active page (always, cached or not)
    console.log('ionViewWillEnter');
  }


  ionViewDidEnter() {
    // Page has fully entered and now is active.
    console.log('ionViewDidEnter');
  }


  ionViewCanLeave(): boolean | Promise<boolean> {
    //antes de salir de esta  pagina, aqui se fija si se puere salir 
    // ES COMO EL GUARD EN ANGULAR 2 
    console.log('ionViewCanLeave');
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true);
      }, 1000);
    });
    return promise;
  }


  ionViewWillLeave() {
    // Page leave and become inactive
    console.log('ionViewWillLeave');
  }


  ionViewDidLeave() {
    // Page finished living and is no inactive
    console.log('ionViewDidLeave');
  }


  ionViewWillUnload() {
    // Page is destroyed (not cached anymore)
    console.log('ionViewWillUnload');
  }



}
