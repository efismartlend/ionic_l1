import { UsersPage } from './../users/users';
import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usersPage = UsersPage;

  constructor(public navCtrl: NavController) {

  }

  onGoToUsers() {
    // this.navCtrl.push(UsersPage, {}, {
    //   animate: true, //Whether or not the transition should animate.
    //   // animation: //What kind of animation should be used.
    //   direction: 'back', // default for push is 'forward'
    //   duration: 2000, // 2 seconds
    //   easing: 'ease-out'
    // })
    this.navCtrl.push(UsersPage, {})
      .catch(error => console.log('Access denied, Argument was ' + error));
  }
}
