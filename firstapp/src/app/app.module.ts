import { BuyoutPage } from './../pages/buyout/buyout';
import { ShopPage } from './../pages/shop/shop';
import { UserPage } from './../pages/users/user/user';
import { UsersPage } from './../pages/users/users';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

const app_array = [
  MyApp,
  HomePage,
  UsersPage,
  UserPage,
  ShopPage,
  BuyoutPage
];

@NgModule({
  declarations: app_array,
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: app_array,
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule { }
